package com.volio.bookmanagement;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

class MyDatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private static final String DATABASE_NAME = "BookLibrary.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME_USER = "user";
    private static final String TABLE_NAME_BOOK = "my_library";

    private static final String COLUMN_NAME = "user_name";
    private static final String COLUMN_PASSWORD = "user_password";

    private static final String COLUMN_ID = "book_id";
    private static final String COLUMN_TITLE = "book_title";
    private static final String COLUMN_AUTHOR = "book_author";
    private static final String COLUMN_CATEGORY = "book_category";

    MyDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryUser = "CREATE TABLE " + TABLE_NAME_USER +
                " (" + COLUMN_NAME + " TEXT PRIMARY KEY, " +
                COLUMN_PASSWORD + " TEXT NOT NULL);";
        String queryBook = "CREATE TABLE " + TABLE_NAME_BOOK +
                " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TITLE + " TEXT NOT NULL, " +
                COLUMN_AUTHOR + " TEXT, " +
                COLUMN_CATEGORY + " TEXT NOT NULL);";
        db.execSQL(queryUser);
        db.execSQL(queryBook);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_BOOK);
        onCreate(db);
    }

    void addUser(String name, String pass){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_NAME, name);
        cv.put(COLUMN_PASSWORD, pass);
        long result = db.insert(TABLE_NAME_USER,null, cv);
        if(result == -1){
            Toast.makeText(context, "Tên đăng nhập đã tồn tại!", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "Đăng ký thành công!", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(context,LoginSuccessActivity.class);
            context.startActivity(intent);
        }
    }

    void addBook(String title, String author, String category){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_TITLE, title);
        cv.put(COLUMN_AUTHOR, author);
        cv.put(COLUMN_CATEGORY, category);
        long result = db.insert(TABLE_NAME_BOOK,null, cv);
        if(result == -1){
            Toast.makeText(context, "Thêm sách thất bại!", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "Thêm sách thành công!", Toast.LENGTH_SHORT).show();
        }
    }

    Cursor readAllUser(){
        String query = "SELECT * FROM " + TABLE_NAME_USER;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        if(db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    Cursor readAllBook(){
        String query = "SELECT * FROM " + TABLE_NAME_BOOK;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = null;
        if(db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    void updateBook(String row_id, String title, String author, String category){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TITLE, title);
        cv.put(COLUMN_AUTHOR, author);
        cv.put(COLUMN_CATEGORY, category);

        long result = db.update(TABLE_NAME_BOOK, cv, "book_id=?", new String[]{row_id});
        if(result == -1){
            Toast.makeText(context, "Cập nhật thất bại!", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "Cập nhật thành công!", Toast.LENGTH_SHORT).show();
        }

    }

    void deleteOneRow(String row_id){
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(TABLE_NAME_BOOK, "book_id=?", new String[]{row_id});
        if(result == -1){
            Toast.makeText(context, "Xóa thất bại!", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "Xóa thành công!", Toast.LENGTH_SHORT).show();
        }
    }

    Cursor searchBookFromNameAndCategory(String key){
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor=database.query(TABLE_NAME_BOOK,null,
                COLUMN_TITLE+" like ? or "+COLUMN_CATEGORY+" like ?",
                new String[]{"%"+key+"%","%"+key+"%"},
                null,null,null);
        return cursor;
    }

    Cursor searchBookFromCateory(String key){
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor=database.query(TABLE_NAME_BOOK,null,
                COLUMN_CATEGORY+" like ?",
                new String[]{"%"+key+"%"},
                null,null,null);
        return cursor;
    }
}
