package com.volio.bookmanagement;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volio.bookmanagement.model.Book;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    List<Book> bookList;
    IOnSelectBook iOnSelectBook;

    public BookAdapter(Context context,List<Book> bookList,IOnSelectBook iOnSelectBook){
        this.context=context;
        this.bookList=bookList;
        this.iOnSelectBook=iOnSelectBook;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context)
                .inflate(R.layout.item_book,parent,false);
        return new ItemBookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemBookViewHolder viewHolder= (ItemBookViewHolder) holder;
        Book book=bookList.get(position);
        viewHolder.onFill(book);
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    class ItemBookViewHolder extends RecyclerView.ViewHolder {
        TextView txtBookId,txtBookTitle,txtBookAuthor,txtBookCategory;
        MyDatabaseHelper myDB = new MyDatabaseHelper(itemView.getContext());

        public ItemBookViewHolder(@NonNull View itemView) {
            super(itemView);
            txtBookId=itemView.findViewById(R.id.txt_book_id);
            txtBookTitle=itemView.findViewById(R.id.txt_book_title);
            txtBookAuthor=itemView.findViewById(R.id.txt_book_author);
            txtBookCategory=itemView.findViewById(R.id.txt_book_category);
        }

        public void onFill(final Book book) {
            txtBookId.setText(String.valueOf(book.getId()));
            txtBookTitle.setText(book.getTitle());
            txtBookAuthor.setText(book.getAuthor());
            txtBookCategory.setText(book.getCategory());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(iOnSelectBook!=null){
                        iOnSelectBook.selectItem(book);
                    }
//                    final Dialog dialog = new Dialog(itemView.getContext());
//                    dialog.setContentView(R.layout.dialog_add_book);
//
//                    final EditText edtTitle = dialog.findViewById(R.id.edt_title);
//                    final EditText edtAuthor = dialog.findViewById(R.id.edt_author);
//                    final EditText edtCategory = dialog.findViewById(R.id.edt_category);
//                    TextView txtAdd = dialog.findViewById(R.id.txt_add);
//                    TextView txtUpdate = dialog.findViewById(R.id.txt_update);
//                    TextView txtDelete = dialog.findViewById(R.id.txt_delete);
//                    txtUpdate.setVisibility(View.GONE);
//                    txtDelete.setVisibility(View.GONE);
//                    txtAdd.setVisibility(View.VISIBLE);
//                    txtAdd.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if(edtTitle.getText().toString().equals("") || edtAuthor.getText().toString().equals("") || edtCategory.getText().toString().equals("")){
//                                Toast.makeText(itemView.getContext(), "Vui lòng nhập đủ thông tin!", Toast.LENGTH_SHORT).show();
//                            }else{
//                                myDB.addBook(edtTitle.getText().toString().trim(),
//                                        edtAuthor.getText().toString().trim(),
//                                        edtCategory.getText().toString().trim());
//                                storeBookInArrays();
//                                dialog.dismiss();
//                            }
//                        }
//                    });
//                    dialog.show();
//                    Window window = dialog.getWindow();
//                    assert window != null;
//                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            });
        }
    }
}
