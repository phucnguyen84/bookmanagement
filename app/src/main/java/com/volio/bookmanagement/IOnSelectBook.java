package com.volio.bookmanagement;

import com.volio.bookmanagement.model.Book;

public interface IOnSelectBook {

    void selectItem(Book book);

}
