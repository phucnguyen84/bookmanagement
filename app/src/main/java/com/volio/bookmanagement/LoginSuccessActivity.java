package com.volio.bookmanagement;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.volio.bookmanagement.model.Book;

import java.util.ArrayList;
import java.util.List;

public class LoginSuccessActivity extends AppCompatActivity implements IOnSelectBook{

    RecyclerView rvBook;
    Spinner spnCategory;
    TextView txtNoBook,txtAddBook;
    LinearLayout llBook;
    List<Book> bookList=new ArrayList<>();
    ArrayAdapter<String> adapterSpin;
    List<String> categoryList=new ArrayList<>();
    BookAdapter adapter=new BookAdapter(this,bookList,this);;
    MyDatabaseHelper myDB = new MyDatabaseHelper(LoginSuccessActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_success);
        txtNoBook=findViewById(R.id.txt_no_book);
        spnCategory=findViewById(R.id.spn_category);
        rvBook=findViewById(R.id.rv_book);
        txtAddBook=findViewById(R.id.txt_add_book);
        llBook=findViewById(R.id.ll_book);
        storeBookInArrays();
        rvBook.setLayoutManager(new LinearLayoutManager(this));

        rvBook.setAdapter(adapter);
        txtAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginSuccessActivity.this);
                dialog.setContentView(R.layout.dialog_add_book);

                final EditText edtTitle = dialog.findViewById(R.id.edt_title);
                final EditText edtAuthor = dialog.findViewById(R.id.edt_author);
                final EditText edtCategory = dialog.findViewById(R.id.edt_category);
                TextView txtAdd = dialog.findViewById(R.id.txt_add);
                TextView txtUpdate = dialog.findViewById(R.id.txt_update);
                TextView txtDelete = dialog.findViewById(R.id.txt_delete);
                txtUpdate.setVisibility(View.GONE);
                txtDelete.setVisibility(View.GONE);
                txtAdd.setVisibility(View.VISIBLE);
                txtAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(edtTitle.getText().toString().equals("") || edtAuthor.getText().toString().equals("") || edtCategory.getText().toString().equals("")){
                            Toast.makeText(LoginSuccessActivity.this, "Vui lòng nhập đủ thông tin!", Toast.LENGTH_SHORT).show();
                        }else{
                            myDB.addBook(edtTitle.getText().toString().trim(),
                                    edtAuthor.getText().toString().trim(),
                                    edtCategory.getText().toString().trim());
                            storeBookInArrays();
                            dialog.dismiss();
                        }
                    }
                });
                dialog.show();
                Window window = dialog.getWindow();
                assert window != null;
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });
        adapterSpin = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, categoryList);
        adapterSpin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCategory.setAdapter(adapterSpin);
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                storeBookInCategory(selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void storeBookInCategory(String key){
        Cursor cursor = myDB.searchBookFromCateory(key);
        bookList.clear();
        if(cursor.getCount() == 0){
            txtNoBook.setVisibility(View.VISIBLE);
            llBook.setVisibility(View.GONE);
        }else{
            txtNoBook.setVisibility(View.GONE);
            llBook.setVisibility(View.VISIBLE);
            while (cursor.moveToNext()){
                String id=cursor.getString(0);
                String title=cursor.getString(1);
                String author=cursor.getString(2);
                String category=cursor.getString(3);
                Book book=new Book(Integer.valueOf(id),title,author,category);
                bookList.add(book);
            }
        }
        adapter.notifyDataSetChanged();
        adapterSpin.notifyDataSetChanged();
    }

    public void storeBookInArrays() {
        Cursor cursor = myDB.readAllBook();
        bookList.clear();
        categoryList.clear();
        categoryList.add("");
        if(cursor.getCount() == 0){
            txtNoBook.setVisibility(View.VISIBLE);
            llBook.setVisibility(View.GONE);
        }else{
            txtNoBook.setVisibility(View.GONE);
            llBook.setVisibility(View.VISIBLE);
            while (cursor.moveToNext()){
                String id=cursor.getString(0);
                String title=cursor.getString(1);
                String author=cursor.getString(2);
                String category=cursor.getString(3);
                categoryList.add(category);
                Book book=new Book(Integer.valueOf(id),title,author,category);
                bookList.add(book);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_search,menu);
        MenuItem mnuSearch=menu.findItem(R.id.mnuSearch);
        final SearchView searchView= (SearchView) mnuSearch.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dataSearch(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    void dataSearch(String key){
        Cursor cursor = myDB.searchBookFromNameAndCategory(key);
        bookList.clear();
        if(cursor.getCount() == 0){
            txtNoBook.setVisibility(View.VISIBLE);
            llBook.setVisibility(View.GONE);
        }else{
            txtNoBook.setVisibility(View.GONE);
            llBook.setVisibility(View.VISIBLE);
            while (cursor.moveToNext()){
                String id=cursor.getString(0);
                String title=cursor.getString(1);
                String author=cursor.getString(2);
                String category=cursor.getString(3);
                Book book=new Book(Integer.valueOf(id),title,author,category);
                bookList.add(book);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void selectItem(final Book book) {
        final Dialog dialog = new Dialog(LoginSuccessActivity.this);
        dialog.setContentView(R.layout.dialog_add_book);

        final EditText edtTitle = dialog.findViewById(R.id.edt_title);
        final EditText edtAuthor = dialog.findViewById(R.id.edt_author);
        final EditText edtCategory = dialog.findViewById(R.id.edt_category);
        TextView txtAdd = dialog.findViewById(R.id.txt_add);
        TextView txtUpdate = dialog.findViewById(R.id.txt_update);
        TextView txtDelete = dialog.findViewById(R.id.txt_delete);
        txtUpdate.setVisibility(View.VISIBLE);
        txtDelete.setVisibility(View.VISIBLE);
        txtAdd.setVisibility(View.GONE);
        edtTitle.setText(book.getTitle());
        edtAuthor.setText(book.getAuthor());
        edtCategory.setText(book.getCategory());
        txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtTitle.getText().toString().equals("") || edtAuthor.getText().toString().equals("") || edtCategory.getText().toString().equals("")){
                    Toast.makeText(LoginSuccessActivity.this, "Vui lòng nhập đủ thông tin!", Toast.LENGTH_SHORT).show();
                }else{
                    myDB.updateBook(String.valueOf(book.getId()),edtTitle.getText().toString(),edtAuthor.getText().toString(),edtCategory.getText().toString());
                    storeBookInArrays();
                    dialog.dismiss();
                }
            }
        });
        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginSuccessActivity.this);
                builder.setTitle("Xóa " + book.getTitle() + " ?");
                builder.setMessage("Bạn có thật sự muốn xóa " + book.getTitle() + " ?");
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        myDB.deleteOneRow(String.valueOf(book.getId()));
                        storeBookInArrays();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.create().show();
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
