package com.volio.bookmanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText edtName,edtPass;
    Button btnRegister,btnLogin;

    MyDatabaseHelper myDB;
    List<String> userName, passWord;

    boolean checkLogin=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtName=findViewById(R.id.edt_name);
        edtPass=findViewById(R.id.edt_pass);
        btnRegister=findViewById(R.id.btn_register);
        btnLogin=findViewById(R.id.btn_login);
        myDB = new MyDatabaseHelper(MainActivity.this);
        userName = new ArrayList<>();
        passWord = new ArrayList<>();
        storeUserInArrays();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtName.getText().toString().equals("") || edtPass.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Vui lòng nhập đủ thông tin!", Toast.LENGTH_SHORT).show();
                }else{
                    for(int i=0; i<userName.size(); i++){
                        if(edtName.getText().toString().equals(userName.get(i)) &&
                                edtPass.getText().toString().equals(passWord.get(i))){
                            checkLogin=true;
                            Intent intent = new Intent(MainActivity.this,LoginSuccessActivity.class);
                            startActivity(intent);
                        }
                    }
                    if(!checkLogin){
                        Toast.makeText(MainActivity.this, "Đăng nhập thất bại!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void storeUserInArrays() {
        Cursor cursor = myDB.readAllUser();
        if(cursor.getCount() > 0){
            while (cursor.moveToNext()){
                userName.add(cursor.getString(0));
                passWord.add(cursor.getString(1));
            }
        }
    }
}
